//Program to calculate the sum and average of any given 3 integers and print the output.
#include <stdio.h>

int main(void)
{
  int i;
  int num , tot = 0 , avg = 0;

  for ( i = 0 ; i < 3 ; i++ )
  {
    
    printf( "Enter a number : " );
    scanf( "%d" , &num );

    tot = tot + num;

  }
  
  avg = tot / 3;

  printf( "\nThe total is %d" , tot );
  printf( "\nThe average is %d" , avg );

  return 0;

}