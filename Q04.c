//program to convert temperature given in celsius to Fahrenheit.
#include <stdio.h>

int main(void)
{
  float c , f;
  
  printf( "Enter the temperature in celsius : " );
  scanf( "%f" , &c );

  f = c * 9 / 5 + 35;

  printf( "The fahrenheit value of celsius %.2f is %.2fF" , c , f ); 
  
  return 0;

}