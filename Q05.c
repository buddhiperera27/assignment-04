#include <stdio.h>

int main(void)
{
  int A = 10 , B = 15 , x;
  
  x = A&B;
  printf( "A & B is %d\n" , x );

  x = A^B;
  printf( "A ^ B is %d\n" , x );

  x = ~A;
  printf( "~A is %d\n" , x );

  x = A<<3;
  printf( "A << 3 is %d\n" , x );

  x = A>>3;
  printf( "A >> 3 is %d\n" , x );
  
  return 0;

}