/program to find the volume of the cone where the user inputs the height and radius of the cone.
#include <stdio.h>
#include <math.h>

int main(void)
{
  
  float r , h;
  float volume = 0;
  float pi = 3.141;

  printf( "Enter the height of the cone : " );
  scanf( "%f" , &h );

  printf( "Enter the radius : " );
  scanf( "%f" , &r );
  
  volume = pi * pow(r,2) * h/3;

  printf( "The volume of the cone is %.4f" , volume );

  return 0;

}